import { combineReducers } from 'redux'

const blog = (state = { blog: {} }, action) => {
  switch(action.type) {
    case 'GET_ONE_BLOG':
      action.blog.comments = action.comments
      return {
        ...state, 
        blog: action.blog
      }
    case 'REMOVE_BLOG': 
      return {
        ...state, 
        blog: { }
      }
    case 'ADD_COMMENT':
      return {
        ...state,
        blog: {
          ...state.blog,
          comments: [...state.blog.comments, action.comment]
        }
      }
    case 'DELETE_COMMENT':
      const comments = [...state.blog.comments]
      const indexComment = comments.findIndex(comment => comment.id === action.id)
      return {
        ...state,
        blog: {
          ...state.blog,
          comments: [...comments.slice(0, indexComment), 
          ...comments.slice(indexComment + 1)]
        }
      }
    case 'EDIT_COMMENT':
      const editComments = [...state.blog.comments]
      const indexEditComment = editComments.findIndex(comment => comment.id === action.id)
      const { body, timestamp } = action.comment
      const newCommentToEdit = Object.assign({}, editComments[indexEditComment], { 
        body,
        timestamp
      })
      return {
        ...state, 
        blog: {
          ...state.blog, 
          comments: [...editComments.slice(0, indexEditComment),
          newCommentToEdit, ...editComments.slice(indexEditComment + 1)]
        }
      }
    case 'UPVOTE_COMMENT':
      const upVoteComments = [...state.blog.comments]
      const indexUpComment = upVoteComments.findIndex(comment => comment.id === action.id)
      const upScore = action.voteScore
      const newUpScore = Object.assign({}, upVoteComments[indexUpComment], { 
        voteScore: upScore 
      })
      return {
        ...state, 
        blog: {
          ...state.blog, 
          comments: [...upVoteComments.slice(0, indexUpComment),
          newUpScore, ...upVoteComments.slice(indexUpComment + 1)]
        }
      }
    case 'DOWNVOTE_COMMENT':
      const downVoteComments = [...state.blog.comments]
      const indexDownComment = downVoteComments.findIndex(comment => comment.id === action.id)
      const downScore = action.voteScore
      const newDownScore = Object.assign({}, downVoteComments[indexDownComment], { 
        voteScore: downScore 
      })
      return {
        ...state, 
        blog: {
          ...state.blog, 
          comments: [...downVoteComments.slice(0, indexDownComment),
          newDownScore, ...downVoteComments.slice(indexDownComment + 1)]
        }
      }
      case 'VOTE_BLOG':
        return {
          ...state, 
          blog: {
            ...state.blog,
            voteScore: action.voteScore
          }
        }
    default: 
      return state
  }
}

// For Blogs
const blogs = (state = { blogs: [] }, action) => {
  switch(action.type) {
    case 'GET_ALL_BLOGS':
      action.blog.comments = action.comments
      return {
        blogs: [...state.blogs, action.blog]
      }
    case 'ADD_NEW_BLOG':
      return {
        ...state,
        blogs: [...state.blogs, action.blog]
      }
    case 'DELETE_BLOGS':
      return {
        blogs: []
    }
    case 'DELETE_BLOG':
      const currentBlog = [...state.blogs]
      const indexToDelete = currentBlog.findIndex(blog => blog.id === action.id)
      return {
        blogs: [...currentBlog.slice(0, indexToDelete), 
        ...currentBlog.slice(indexToDelete + 1)]
      }
    case 'EDIT_BLOG':
      const currentData = [...state.blogs]
      const indexEdit = currentData.findIndex(blog => blog.id === action.id)
      const { title, category, body, author } = action.blog
      const newBlogToEdit = Object.assign({}, currentData[indexEdit], {
        title,
        category,
        body,
        author
      })
      return {
        blogs: [...currentData.slice(0, indexEdit),
        newBlogToEdit, ...currentData.slice(indexEdit + 1)]
      }
    case 'UP_VOTE':
      const currentBlogUpVote = [...state.blogs]
      const indexUp= currentBlogUpVote.findIndex(blog => blog.id === action.id)
      currentBlogUpVote[indexUp].voteScore = currentBlogUpVote[indexUp].voteScore + 1
      return {
        blogs: [...currentBlogUpVote]
      }
    case 'DOWN_VOTE':
      const currentBlogDownVote = [...state.blogs]
      const indexDown= currentBlogDownVote.findIndex(blog => blog.id === action.id)
      currentBlogDownVote[indexDown].voteScore = currentBlogDownVote[indexDown].voteScore - 1
      return {
        blogs: [...currentBlogDownVote]
      }
    case 'GET_BLOG_CATEGORY':
      return {
        ...state,
        blogs: action.blogs
      }
    default:
      return state
  }
}

// For Categories
const categories = (state = { categories: [] }, action) => {
  switch(action.type) {
    case 'GET_CATEGORIES':
      return {
        ...state,
        categories: action.categories
      }
    default:
      return state
  }
}

const sort = (state = { sort: 'popular' }, action) => {
  switch(action.type) {
    case 'CHANGE_SORT':
      const newValue = action.value
      return {
        ...state, 
        sort: newValue
      }
    default: 
      return state
  }
}

export default combineReducers({
  blog,
  blogs,
  categories,
  sort
})
