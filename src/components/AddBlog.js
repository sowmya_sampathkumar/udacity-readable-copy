import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCategories, addNewBlogAction } from '../actions'

const elmStyle = {
  height:'100%',
  verticalAlign:'middle',
  margin:'10px',
};

class AddBlog extends Component {
  state = {
    title: '',
    category: '',
    author: '',
    body: '',
  }

  componentDidMount() {
    this.props.getCategories();
  }

  onBlogClick() {
    let { title, category, author, body } = this.state

    if (title && category && author && body) {
      const addBlog = {
        id: Math.random().toString(36).substr(-8),
        timestamp: Date.now(),
        title,
        category,
        author,
        body
      } 
      console.log(addBlog)
      this.props.addBlog(addBlog)
        .then(() => this.setState({
          title: '',
          category: '',
          author: '',
          body: '',
        }))
        alert('Blog added successfully');
      } else {
      alert('Please input valid data');
    }
  }

  onTitleChange(e) {
    this.setState({ title: e.target.value })
  }

  onAuthorChange(e) {
    this.setState({ author: e.target.value })
  }

  onBodyChange(e) {
    this.setState({ body: e.target.value })
  }

  onCategoryChange = (e) => {
    this.setState({
      category: e.target.value
    })
  }

  render() {
    const { categories } = this.props
    const catgList = categories.map(opt => ( 
        <option 
          key={opt.name} 
          value={opt.name}>{opt.name}</option>
    ))
    return(
      <div className="New-Blog">
        <input type="text" style={elmStyle} placeholder="Input Blog Title" onChange={(e) => this.onTitleChange(e)} value={this.state.title}></input>
        <input placeholder="Created by" style={elmStyle} onChange={(e) => this.onAuthorChange(e)} type="text" value={this.state.author}></input>
        <select value={this.state.category} onChange={this.onCategoryChange}> {catgList}</select>
        <textarea placeholder="Input Blog content" style={elmStyle} onChange={(e) => this.onBodyChange(e)} value={this.state.body} name="body" cols="100" rows="3"> </textarea>
        <input className="Blog-Button" style={elmStyle} type="button" value="Blog" onClick={this.onBlogClick.bind(this)}></input>
      </div>
    )
  }
}

const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.categories
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(fetchCategories()),
    addBlog: (blog) => dispatch(addNewBlogAction(blog))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddBlog)
