import React, { Component } from 'react'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class Sort extends Component {
  setSortClick(stype) {
    //console.log(stype)
    this.props.sort(stype)
  }

  render() {    
    return(
      <div className="Sorting">
        <span>Click to select sorting order -></span>
        <input key= 'Votes' className="vote-Button" type="button" style={elmStyle} onClick={this.setSortClick.bind(this, 'vote')} value="Votes"></input>
        <input key= 'Times' className="time-Button" type="button" style={elmStyle} onClick={this.setSortClick.bind(this, 'time')} value="Timestamp"></input>
      </div>
    )
  }
}

export default Sort
