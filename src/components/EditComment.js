import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getBlog, editCommentAction } from '../actions'

const elmStyle = {
  aligh:'center',
  margin:'5px',
};

class EditComment extends Component {
  state = {
    index: '',
    body: '',
    timestamp: '',
    commentid: ''
  }

  componentDidMount() {
    const { index } = this.props.match.params
    const { blog } = this.props.blog
    const { comments } = blog

    this.setState({ index: index })
    this.setState({ commentid: comments[index].id })
    this.setState({ body: comments[index].body })
  }

  onSaveClick() {
    let { index, body } = this.state

    if (!index && !body) {
      alert('Please input valid data');
    } else {
      const updBlogData = {
        timestamp: Date.now(),
        body
      } 
      var newPath = '/blog/'+ this.props.blog.blog.id;
      //console.log(this.state.commentid)
      //console.log(updBlogData)
      this.props.updComment(this.state.commentid, updBlogData)
        .then(
          alert('Blog updated successfully')
        )
        this.props.history.push(newPath)
    }
  }

  onBodyChange(e) {
    this.setState({ body: e.target.value })
  }

  render() {
    return(
      <div className="EditComment">
        <div><textarea placeholder="Update comments here..." value={this.state.body} name="body" onChange={(e) => this.onBodyChange(e)} style={elmStyle} cols="100" rows="6"></textarea></div>
        <div><input style={elmStyle} className="Save-Button" type="button" value="Save changes" onClick={this.onSaveClick.bind(this)}></input></div>
      </div>
    )
  }
}

const mapStateToProps = ({blog, comments }) => {
  return {
    blog: blog,
    comments: comments,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBlog: (id) => dispatch(getBlog(id)),
    updComment: (id, blog) => dispatch(editCommentAction(id, blog))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditComment)
