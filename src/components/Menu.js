import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { fetchCategories } from '../actions'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class Menu extends Component {
  componentDidMount() {
    this.props.getCategories()
  }

  render() {
    const { categories } = this.props
    return(
      <div className="Categories">
        <ul id="menu">
          <span>Click on any category to filter by -></span>
          {categories.map((item, index) => {
            const catgName = item.name
            return (
              <li key={index} style={elmStyle} ><Link to={`/${item.name}`}>{item.name}</Link></li>
            )
          })}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.categories,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(fetchCategories())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
