import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import Menu from './Menu.js'
import Sort from './Sort.js'
import NewBlog from './AddBlog.js'
import Blogs from './Blogs.js'

class MainPage extends Component {
  state = {
    filter: 'all',
    sortorder:'',
  }
  
  changeFilter(ftype) {
    this.setState({
      filter:ftype
    })
  }

  changeSort(stype) {
    this.setState({
      sortorder:stype
    })
  }

  render() {
    return (
      <div className="Main">
        <Menu filter={this.changeFilter.bind(this)}/>
        <Sort sort={this.changeSort.bind(this)}/>
        <hr />
        <NewBlog />
        <hr />
        <Blogs filter={this.state.filter} sort={this.state.sortorder}/>
      </div>
    );
  }
}

export default withRouter(MainPage)
