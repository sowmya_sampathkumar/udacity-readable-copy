import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getBlogs, upVoteAction, downVoteAction, deleteBlogAction } from '../actions'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class Blogs extends Component {
  componentDidMount() {
    this.props.getBlogs()
  }

  onUpCommentClick(index) {
    this.props.addVote(this.props.blogs[index].id)
      .then(
        alert("Up Voted successfully")
      )
  }

  onDnCommentClick(index) {
    this.props.delVote(this.props.blogs[index].id)
      .then(
        alert("Down voted successfully")
      )
  }

  onDelPostClick(index) {
    this.props.delPost(this.props.blogs[index].id)
      .then(
        alert("Post deleted successfully")
      )
  }

  render() {
    //console.log(this.props.blogs)

    const {blogs}  = this.props
    const {filter}  = this.props
    const {sort} = this.props

    switch (sort) {
      case 'vote':
        blogs.sort((a, b) => a.voteScore < b.voteScore)
        break;
      case 'time':
        blogs.sort((a, b) => a.timestamp < b.timestamp)
        break;
      default:
        break;
    }
    return(
      <div className="Blogs">
        <ul id = "blogs" className="Blogs-List">
          {blogs.map((blog, index) => {
            let bcomments = [];
            if (blog.comments !== undefined){
              bcomments = blog.comments
            }
            var bdate = new Date(blog.timestamp);
            if (filter !== 'all')  {
              if (blog.category === filter) {
                return (
                  <div key={"div"+index}>
                    <li key={index}><Link to={`/${blog.category}/${blog.id}`}>{blog.title}</Link></li>
                    <li key={index+'1'}><b>Created by : </b>{blog.author} <b>on</b> {bdate.toString()} <b>under</b> {blog.category}</li>
                    <li key={index+'2'}>{blog.body}</li>
                    <li key={index+'3'}>Votes = {blog.voteScore} Comments = {bcomments.length}</li>
                    <input className="Uv-Button" type="button" style={elmStyle} onClick={this.onUpCommentClick.bind(this, index)} value="Up vote"></input>
                    <input className="Dv-Button" type="button" style={elmStyle} onClick={this.onDnCommentClick.bind(this, index)} value="Down vote"></input>
                    <button><Link to={`/edit/${blog.id}`}>Edit Post</Link></button>
                    <hr/>
                  </div>
                )
              }
            } else {
              return (
              <div key={"div"+index}>
                <li key={index}><Link to={`/${blog.category}/${blog.id}`}>{blog.title}</Link></li>
                <li key={index+'1'}><b>Created by : </b>{blog.author} <b>on</b> {bdate.toString()} <b>under</b> {blog.category}</li>
                <li key={index+'2'}>{blog.body}</li>
                <li key={index+'3'}>Votes = {blog.voteScore} Comments = {bcomments.length}</li>
                <input className="Uv-Button" type="button" style={elmStyle} onClick={this.onUpCommentClick.bind(this, index)} value="Up vote"></input>
                <input className="Dv-Button" type="button" style={elmStyle} onClick={this.onDnCommentClick.bind(this, index)} value="Down vote"></input>
                <button><Link to={`/edit/${blog.id}`}>Edit Post</Link></button>
                <input className="Del-Button" type="button" style={elmStyle} onClick={this.onDelPostClick.bind(this, index)} value="Delete Blog"></input>
                <hr/>
              </div>)
            }
          })}
        </ul>
      </div>
    )
  }
}
const mapStateToProps = ({ blogs }) => {
  return {
    blogs: blogs.blogs,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBlogs: () => dispatch(getBlogs()),
    addVote: (id) => dispatch(upVoteAction(id)),
    delVote: (id) => dispatch(downVoteAction(id)),
    delPost: (id) => dispatch(deleteBlogAction(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Blogs)
