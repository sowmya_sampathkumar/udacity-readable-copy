import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getBlogs, upVoteAction, downVoteAction, deleteBlogAction } from '../actions'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class Blogs extends Component {
  state = {
    filter: 'all'
  }

  componentDidMount() {
    const { catg} = this.props.match.params
    this.state.filter = catg
    console.log(catg)
    this.props.getBlogs()
  }

  onUpCommentClick(index) {
    this.props.addVote(this.props.blogs[index].id)
      .then(
        alert("Up Voted successfully")
      )
  }

  onDnCommentClick(index) {
    this.props.delVote(this.props.blogs[index].id)
      .then(
        alert("Down voted successfully")
      )
  }

  onDelPostClick(index) {
    this.props.delPost(this.props.blogs[index].id)
      .then(
        alert("Post deleted successfully")
      )
  }

  render() {
    const {blogs}  = this.props
    console.log(this.state.filter)

    return(
      <div className="Blogs">
        <ul id = "blogs" className="Blogs-List">
          {blogs.map((blog, index) => {
            let bcomments = [];
            if (blog.comments !== undefined){
              bcomments = blog.comments
            }
            var bdate = new Date(blog.timestamp);
            if (blog.category === this.state.filter) {
              return (
                <div key={"div"+index}>
                  <li key={index}><Link to={`/${blog.category}/${blog.id}`}>{blog.title}</Link></li>
                  <li key={index+'1'}><b>Created by : </b>{blog.author} <b>on</b> {bdate.toString()} <b>under</b> {blog.category}</li>
                  <li key={index+'2'}>{blog.body}</li>
                  <li key={index+'3'}>Votes = {blog.voteScore} Comments = {bcomments.length}</li>
                  <input className="Uv-Button" type="button" style={elmStyle} onClick={this.onUpCommentClick.bind(this, index)} value="Up vote"></input>
                  <input className="Dv-Button" type="button" style={elmStyle} onClick={this.onDnCommentClick.bind(this, index)} value="Down vote"></input>
                  <button><Link to={`/edit/${blog.id}`}>Edit Post</Link></button>
                  <hr/>
                </div>
              )
            }
          })}
        </ul>
      </div>
    )
  }
}
const mapStateToProps = ({ blogs }) => {
  return {
    blogs: blogs.blogs,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBlogs: () => dispatch(getBlogs()),
    addVote: (id) => dispatch(upVoteAction(id)),
    delVote: (id) => dispatch(downVoteAction(id)),
    delPost: (id) => dispatch(deleteBlogAction(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Blogs)
