import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getBlog, fetchCategories, editBlogAction } from '../actions'

const elmStyle = {
  aligh:'center',
  margin:'5px',
};

class EditBlog extends Component {
  state = {
    id: '',
    author: '',
    body: '',
    deleted: false,
    parentDeleted: false,
    parentId: '',
    timestamp: '',
    BlogvoteScore: 1,
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.props.getBlog(id);
    const { blog } = this.props.blog
    //console.log(blog)
    this.setState({ id: id })
    this.setState({ title: blog.title })
    this.setState({ author: blog.author })
    this.setState({ category: blog.category })
    this.setState({ body: blog.body })
    this.setState({ parentId: blog.parentId })
    //console.log(this.state)

    this.props.getCategories();
  }

  onSaveClick() {
    let { id, title, category, author, body } = this.state

    if (!id && !title && !category && !author && !body) {
      alert('Please input valid data');
    } else {
      const updBlogData = {
        timestamp: Date.now(),
        title,
        category,
        author,
        body
      } 
      var newPath = '/blog/'+ id;
      this.props.updBlog(id, updBlogData)
        .then(
          alert('Blog updated successfully')
        )
        this.props.history.push(newPath)
    }
  }

  onTitleChange(e) {
    this.setState({ title: e.target.value })
  }

  onAuthorChange(e) {
    this.setState({ author: e.target.value })
  }

  onBodyChange(e) {
    this.setState({ body: e.target.value })
  }

  onCategoryChange = (e) => {
    this.setState({category: e.target.value})
  }

  render() {
    const { categories } = this.props
    const catgList = categories.map(opt => ( 
        <option 
          key={opt.name} 
          value={opt.name}>{opt.name}</option>
    ))

    return(
      <div className="EditBlog">
        <div><input type="text" style={elmStyle} placeholder="Input Blog Title" onChange={(e) => this.onTitleChange(e)} value={this.state.title}></input></div>
        <div><input style={elmStyle} placeholder="Created by" onChange={(e) => this.onAuthorChange(e)} type="text" value={this.state.author}></input></div>
        <div><select style={elmStyle} value={this.state.category} onChange={this.onCategoryChange}> {catgList}</select></div>
        <div><textarea style={elmStyle} placeholder="Input Blog content" onChange={(e) => this.onBodyChange(e)} value={this.state.body} name="body" cols="100" rows="3"></textarea></div>
        <div><input style={elmStyle} className="Save-Button" type="button" value="Save changes" onClick={this.onSaveClick.bind(this)}></input></div>
      </div>
    )
  }
}

const mapStateToProps = ({blog, categories }) => {
  return {
    blog: blog,
    categories: categories.categories
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBlog: (id) => dispatch(getBlog(id)),
    getCategories: () => dispatch(fetchCategories()),
    updBlog: (id, blog) => dispatch(editBlogAction(id, blog))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBlog)
