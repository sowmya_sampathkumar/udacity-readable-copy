import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { deleteCommentAction, upVoteCommentAction, downVoteCommentAction } from '../actions'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class CommentList extends Component {
  onDelCommentClick(index) {
    this.props.delComment(this.props.comments[index].id)
      .then(
        alert("Comment deleted successfully")
      )
  }

  onUpCommentClick(index) {
    this.props.upVote(this.props.comments[index].id)
      .then(
        alert("Up Voted successfully")
      )
  }

  onDnCommentClick(index) {
    this.props.downVote(this.props.comments[index].id)
      .then(
        alert("Down voted successfully")
      )
  }

  render() { 
    let { comments } = this.props
    if (comments === undefined){
      comments = [];
    }
    return(
      <div>
        <ul id = "blogs" className="Comments-List">
          {comments.map((comment, index) => {
            return (
              <div key={"commkey"+index}>
                <li key={index}>{comment.body}</li>
                <li key={index+'1'}>Votes = {comment.voteScore} </li>
                <button><Link to={`/comment/${index}`}>Edit comment</Link></button>
                <input className="Del-Button" type="button" style={elmStyle} onClick={this.onDelCommentClick.bind(this, index)} value="Delete Comment"></input>
                <input className="Uv-Button" type="button" style={elmStyle} onClick={this.onUpCommentClick.bind(this, index)} value="Up vote"></input>
                <input className="Dv-Button" type="button" style={elmStyle} onClick={this.onDnCommentClick.bind(this, index)} value="Down vote"></input>
                <hr/>
              </div>
            )
          })}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = ({comment }) => {
  return {
    comment: comment,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    delComment: (id) => dispatch(deleteCommentAction(id)),
    upVote: (id) => dispatch(upVoteCommentAction(id)),
    downVote: (id) => dispatch(downVoteCommentAction(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentList)
