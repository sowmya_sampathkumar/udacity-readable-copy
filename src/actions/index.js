import { 
  getOneBlog,
  getAllBlogs, 
  getAllCategories,
  addNewBlog,
  editBlog,
  deleteBlog,
  voteBlog,
  getComments,
  addComment,
  deleteComment,
  editComment,
  voteComment
} from '../utils/server'

const GET_ONE_BLOG = 'GET_ONE_BLOG'
const GET_ALL_BLOGS = 'GET_ALL_BLOGS'
const GET_CATEGORIES = 'GET_CATEGORIES'
const ADD_NEW_BLOG = 'ADD_NEW_BLOG'
const EDIT_BLOG = 'EDIT_BLOG'
const DELETE_BLOG = 'DELETE_BLOG'
const VOTE_BLOG = 'VOTE_BLOG'
const DOWN_VOTE = 'DOWN_VOTE'
const UP_VOTE = 'UP_VOTE'
const ADD_COMMENT = 'ADD_COMMENT'
const DELETE_COMMENT = 'DELETE_COMMENT'
const EDIT_COMMENT = 'EDIT_COMMENT'
const UPVOTE_COMMENT = 'UPVOTE_COMMENT'
const DOWNVOTE_COMMENT = 'DOWNVOTE_COMMENT'

export const getBlogs = () => dispatch => (
  getAllBlogs()
    .then(blogs => {
      blogs.map(blog => {
        getComments(blog.id)
          .then(comments => {
            dispatch({
              type: GET_ALL_BLOGS,
              blog,
              comments
            })
          })
      })
    })
)

export const getBlog = (id) => dispatch => (
  getOneBlog(id)
    .then(blog => {
      getComments(blog.id)
        .then(comments => {
          dispatch({
            type: GET_ONE_BLOG,
            blog,
            comments
          })
        })
    })
)

export const addNewBlogAction = (blog) => dispatch => (
  addNewBlog(blog)
    .then(blog => {
      dispatch({
        type: ADD_NEW_BLOG,
        blog
      })
    })
)

export const editBlogAction = (id, blog) => dispatch => (
  editBlog(id, blog)
    .then((blog) => {
      dispatch({
        type: EDIT_BLOG,
        id, 
        blog
      })
    })
)

export const deleteBlogAction = (id) => dispatch => (
  deleteBlog(id)
    .then(() => {
      dispatch({
        type: DELETE_BLOG,
        id
      })
    })
)

export const downVoteAction = (id) => dispatch => (
  voteBlog(id, "downVote")
    .then(() => {
      dispatch({
        type: DOWN_VOTE,
        id
      })
    })
)

export const upVoteAction = (id) => dispatch => (
  voteBlog(id, "upVote")
    .then(() => {
      dispatch({
        type: UP_VOTE,
        id
      })
    })
)

export const downVoteBlogAction = (id) => dispatch => (
  voteBlog(id, "downVote")
    .then((comment) => {
      dispatch({
        type: VOTE_BLOG,
        voteScore: comment.voteScore
      })
    })
)

export const upVoteBlogAction = (id) => dispatch => (
  voteBlog(id, "upVote")
    .then((comment) => {
      dispatch({
        type: VOTE_BLOG,
        voteScore: comment.voteScore
      })
    })
)

export const getCategories = (categories) => ({
    type: GET_CATEGORIES,
    categories
})

export const fetchCategories = () => dispatch => (
  getAllCategories()
    .then(categories => dispatch(getCategories(categories)))
)

export const addCommentAction = (comment) => dispatch => {
  return addComment(comment)
    .then(comment => {
      dispatch({
        type: ADD_COMMENT,
        comment
      })
    })
}

export const deleteCommentAction = (id) => dispatch => {
  return deleteComment(id)
    .then(() => {
      dispatch({
        type: DELETE_COMMENT,
        id
      })
    })
}

export const editCommentAction = (id, comment) => dispatch => {
  return editComment(id, comment)
    .then((comment) => {
      dispatch({
        type: EDIT_COMMENT,
        id,
        comment
      })
    })
}

export const upVoteCommentAction = (id) => dispatch => (
  voteComment(id, "upVote")
    .then((comment) => {
      dispatch({
        type: UPVOTE_COMMENT,
        id: comment.id,
        parentId: comment.parentId,
        voteScore: comment.voteScore
      })
    })
)

export const downVoteCommentAction = (id) => dispatch => (
  voteComment(id, "downVote")
    .then((comment) => {
      dispatch({
        type: DOWNVOTE_COMMENT,
        id: comment.id,
        parentId: comment.parentId,
        voteScore: comment.voteScore
      })
    })
)
